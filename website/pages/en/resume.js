const React = require('react');
import { Document, Page } from 'react-pdf';

class ResumePage extends React.Component {
  state = {
    numPages: null,
    pageNumber: 1,
  }

  onDocumentLoadSuccess = ({ numPages }) => {
    this.setState({ numPages });
  }

  render() {
    const siteConfig = this.props.config;
    const { pageNumber, numPages } = this.state
    return <div>{siteConfig.title}</div>;
  }
}

ResumePage.title = "Resume";

module.exports = ResumePage;